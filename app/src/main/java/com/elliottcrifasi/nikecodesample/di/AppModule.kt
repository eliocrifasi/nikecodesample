package com.elliottcrifasi.nikecodesample.di

import com.elliottcrifasi.nikecodesample.ui.main.MainViewModel
import com.elliottcrifasi.nikecodesample.network.SearchClient
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


var AppModule = module {
    factory { OkHttpClient.Builder() }
    single<SearchClient> {
        Retrofit.Builder()
            .baseUrl("https://mashape-community-urban-dictionary.p.rapidapi.com/")
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create()))
            .build()
            .create(SearchClient::class.java)
    }
    viewModel { MainViewModel(get()) }
}