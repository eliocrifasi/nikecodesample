package com.elliottcrifasi.nikecodesample.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.elliottcrifasi.nikecodesample.R
import com.elliottcrifasi.nikecodesample.utils.TextUtils
import com.elliottcrifasi.nikecodesample.model.SearchResult
import kotlinx.android.synthetic.main.search_result_list_item.view.*

class SearchResultsAdapter: RecyclerView.Adapter<SearchResultsAdapter.SearchResultViewHolder>() {

    class SearchResultViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val wordTextView = itemView.wordTextView
        val descripionTextView = itemView.descriptionTextView
        val exampleTextView = itemView.exampleTextView
        val thumbsUpTextView = itemView.thumbsUpTextView
        val thumbsDownTextView = itemView.thumbsUpTextView
    }

    var data: List<SearchResult> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchResultViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.search_result_list_item, parent, false)
        return SearchResultViewHolder(
            view
        )
    }

    override fun getItemCount(): Int {
        return data.count()
    }

    override fun onBindViewHolder(holder: SearchResultViewHolder, position: Int) {
        val currentItem = data[position]
        with(holder) {
            wordTextView.text = currentItem.word.toLowerCase()
            descripionTextView.text = TextUtils.sanitizeUDText(currentItem.definition)
            exampleTextView.text = TextUtils.sanitizeUDText(currentItem.example)
            thumbsDownTextView.text = currentItem.thumbs_down.toString()
            thumbsUpTextView.text = currentItem.thumbs_up.toString()
        }
}

}