package com.elliottcrifasi.nikecodesample.ui.main

import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.elliottcrifasi.nikecodesample.R
import com.elliottcrifasi.nikecodesample.adapters.SearchResultsAdapter
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val mainViewModel: MainViewModel by viewModel()
    private val searchResultsAdapter = SearchResultsAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()
        setUp()
    }
    private fun setUp() {
        fun setUpResultsList() {
            searchResultsList.adapter = searchResultsAdapter
            searchResultsList.layoutManager = LinearLayoutManager(this)
            searchResultsList.addItemDecoration(DividerItemDecoration(this, RecyclerView.VERTICAL))
        }
        fun setUpSearchView() = searchView.setOnQueryTextListener(onQueryTextListener)
        fun setUpStateObserver() {
            mainViewModel.searchState.observe(this, Observer {
                when (it) {
                    is MainViewModel.SearchState.Loading -> {
                        searchPrompt.visibility = GONE
                        progressBar.visibility = VISIBLE
                    }
                    is MainViewModel.SearchState.Completed -> {
                        searchResultsAdapter.data = it.searchResponse.list
                        progressBar.visibility = GONE
                    }
                }
            })
        }
        setUpResultsList()
        setUpSearchView()
        setUpStateObserver()
    }

    private val onQueryTextListener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextChange(newText: String?): Boolean = true
        override fun onQueryTextSubmit(query: String?): Boolean {
            searchPrompt.visibility = if (query?.isEmpty() == true) VISIBLE else GONE
            query?.let { mainViewModel.search(it) }
            return true
        }
    }
}
