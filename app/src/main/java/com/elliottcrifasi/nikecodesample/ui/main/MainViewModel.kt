package com.elliottcrifasi.nikecodesample.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.elliottcrifasi.nikecodesample.model.SearchResponse
import com.elliottcrifasi.nikecodesample.network.SearchClient
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel(
    private val searchClient: SearchClient
): ViewModel() {

    sealed class SearchState {
        object Loading: SearchState()
        class Completed(val searchResponse: SearchResponse): SearchState()
    }

    private var _searchState = MutableLiveData<SearchState>()
    val searchState: LiveData<SearchState>
        get() = _searchState

    fun search(query: String) {
        viewModelScope.launch(Dispatchers.IO) {
            _searchState.postValue(SearchState.Loading)
            _searchState.postValue(
                SearchState.Completed(
                    searchClient.search(query)
                )
            )
        }
    }
}