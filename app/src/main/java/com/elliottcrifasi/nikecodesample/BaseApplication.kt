package com.elliottcrifasi.nikecodesample

import android.app.Application
import com.elliottcrifasi.nikecodesample.di.AppModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class BaseApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@BaseApplication)
            androidLogger()
            modules(AppModule)
        }
    }
}