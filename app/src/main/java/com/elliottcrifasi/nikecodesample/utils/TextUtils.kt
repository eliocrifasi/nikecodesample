package com.elliottcrifasi.nikecodesample.utils

object TextUtils {
    fun sanitizeUDText(text: String): String = text.replace("[", "").replace("]","")
}