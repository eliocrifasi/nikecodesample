package com.elliottcrifasi.nikecodesample.model

data class SearchResponse(val list: List<SearchResult>)