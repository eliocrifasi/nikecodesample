package com.elliottcrifasi.nikecodesample.model

import java.util.*

data class SearchResult(
    val definition: String,
    val permalink: String,
    val thumbs_up: Int,
    val word: String,
    val defid: Int,
    val written_on: Date,
    val example: String,
    val thumbs_down: Int
)