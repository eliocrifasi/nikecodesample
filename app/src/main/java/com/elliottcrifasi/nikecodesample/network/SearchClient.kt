package com.elliottcrifasi.nikecodesample.network

import com.elliottcrifasi.nikecodesample.model.SearchResponse
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface SearchClient {
    @Headers(
     "x-rapidapi-host: mashape-community-urban-dictionary.p.rapidapi.com",
     "x-rapidapi-key: d1816164d6msh464e67ad354813cp19ba07jsn81fe5a8c66bf"
    )
    @GET("/define")
    suspend fun search(@Query("term") term: String): SearchResponse
}