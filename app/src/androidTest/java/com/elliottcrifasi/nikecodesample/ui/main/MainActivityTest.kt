package com.elliottcrifasi.nikecodesample.ui.main

import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.elliottcrifasi.nikecodesample.R
import com.elliottcrifasi.nikecodesample.model.SearchResponse
import com.elliottcrifasi.nikecodesample.network.SearchClient
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.test.KoinTest
import org.koin.test.mock.declare

@LargeTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest: KoinTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Before
    fun setUp() {
        val searchClient = object : SearchClient {
            override suspend fun search(term: String): SearchResponse {
                return SearchResponse(listOf())
            }
        }
        declare { single { searchClient } }
    }

    @Test
    fun mainActivityTest() {

        val search = mActivityTestRule.activity.findViewById<android.widget.SearchView>(R.id.searchView)
        val progressBar = onView(withId(R.id.progressBar))
        onView(withId(R.id.searchPrompt)).check { view, _ -> view.visibility = VISIBLE }
        progressBar.check { view, _ -> view.visibility = GONE }
        mActivityTestRule.activity.runOnUiThread {
            search.setQuery("hat", true)
        }
        onView(withId(R.id.searchPrompt)).check { view, _ -> view.visibility = VISIBLE }
    }
}
