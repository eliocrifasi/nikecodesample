package com.elliottcrifasi.nikecodesample

import com.elliottcrifasi.nikecodesample.utils.TextUtils
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class TextUtilsTest {

    @Test
    fun testSanitizer() {
        val string = "A hat is [super] sweet"
        val expected = "A hat is super sweet"
        assert(TextUtils.sanitizeUDText(string) == expected)
    }
}