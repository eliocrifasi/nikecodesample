package com.elliottcrifasi.nikecodesample

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.elliottcrifasi.nikecodesample.di.AppModule
import com.elliottcrifasi.nikecodesample.model.SearchResponse
import com.elliottcrifasi.nikecodesample.model.SearchResult
import com.elliottcrifasi.nikecodesample.network.SearchClient
import com.elliottcrifasi.nikecodesample.ui.main.MainViewModel
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.inject
import org.koin.test.mock.declare
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import java.util.*

@RunWith(JUnit4::class)
class MainViewModelTests: KoinTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var observer: Observer<MainViewModel.SearchState>

    private val mockSearchResult = SearchResult(
        definition = "",
        permalink = "",
        thumbs_up = 1,
        word = "hats",
        defid = 0,
        written_on = Date(),
        example = "A hat is cool",
        thumbs_down = 0
    )
    val mockSearchResponse = SearchResponse(listOf(mockSearchResult))

    @Before
    fun setUp() {
        startKoin {
            modules(AppModule)
        }
        MockitoAnnotations.initMocks(this)
    }

    @After
    fun cleanUp() {
        stopKoin()
    }

    @Test
    fun testMainViewModel() {
        val searchClient = object : SearchClient {
            override suspend fun search(term: String): SearchResponse = mockSearchResponse
        }
        declare {
            single { searchClient }
        }
        val mainViewModel: MainViewModel by inject()
        mainViewModel.searchState.observeForever(observer)
        mainViewModel.search("hats")
        verify(observer, timeout(1000)).onChanged(argThat { it is MainViewModel.SearchState.Loading })
        verify(observer, timeout(1000)).onChanged(argThat { it is MainViewModel.SearchState.Completed })
    }

}